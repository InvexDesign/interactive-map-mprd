<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'frontend.index', 'uses' => 'FrontendController@interactiveMap']);
Route::post('/filter', ['as' => 'frontend.filter', 'uses' => 'FrontendController@filter']);
Route::post('/process-facility-click', ['as' => 'frontend.process_facility_click', 'uses' => 'FrontendController@processFacilityClick']);


if(config('app.env') == 'local')
{
	Route::get('test', function ()
	{
		return 'nope';
	});
	Route::get('sheet', function ()
	{
		return view('frontend.sheet');
	});
	Route::get('debug', function ()
	{
		return 'debugging';
	});
}

Route::get('/login', ['as' => 'session.login', 'uses' => 'SessionController@create']);
Route::post('/login', ['as' => 'session.store', 'uses' => 'SessionController@store']);
Route::get('/logout', ['as' => 'session.destroy', 'uses' => 'SessionController@destroy']);

Route::get('/password/forgot', ['as' => 'password.reset.email.get', 'uses' => 'Auth\PasswordController@getEmail']);
Route::post('/password/forgot', ['as' => 'password.reset.email.post', 'uses' => 'Auth\PasswordController@postEmail']);
Route::get('/password/reset/{token}', ['as' => 'password.reset.get', 'uses' => 'Auth\PasswordController@getReset']);
Route::post('/password/reset', ['as' => 'password.reset.post', 'uses' => 'Auth\PasswordController@postReset']);

Route::get('/unauthorized', ['as' => 'errors.unauthorized', 'uses' => 'ErrorController@unauthorized']);

Route::group(['prefix' => '/manage/', 'namespace' => 'Backend'], function ()
{
	Route::get('/', ['as' => 'backend.dashboard.index', 'uses' => 'DashboardController@index']);
//	Route::get('/', ['as' => 'backend.dashboard.index', 'uses' => '\App\Http\Controllers\FrontendController@interactiveMap']);

	Route::group(['prefix' => '/my-account/'], function ()
	{
		Route::get('/', ['as' => 'backend.my_account.show', 'uses' => 'MyAccountController@show']);
		Route::get('/edit', ['as' => 'backend.my_account.edit.get', 'uses' => 'MyAccountController@getEdit']);
		Route::post('/edit', ['as' => 'backend.my_account.edit.post', 'uses' => 'MyAccountController@postEdit']);
		Route::get('/change-password', ['as' => 'backend.my_account.change_password.get', 'uses' => 'MyAccountController@getChangePassword']);
		Route::post('/change-password', ['as' => 'backend.my_account.change_password.post', 'uses' => 'MyAccountController@postChangePassword']);
	});

	Route::group(['prefix' => '/features/'], function ()
	{
		Route::get('/', ['as' => 'backend.features.index', 'uses' => 'FeatureController@index']);
		Route::get('/create', ['as' => 'backend.features.create.get', 'uses' => 'FeatureController@getCreate']);
		Route::post('/create', ['as' => 'backend.features.create.post', 'uses' => 'FeatureController@postCreate']);
		Route::get('/{id}', ['as' => 'backend.features.show', 'uses' => 'FeatureController@show']);
		Route::get('/{id}/edit', ['as' => 'backend.features.edit.get', 'uses' => 'FeatureController@getEdit']);
		Route::post('/{id}/edit', ['as' => 'backend.features.edit.post', 'uses' => 'FeatureController@postEdit']);
		Route::get('/{id}/destroy', ['as' => 'backend.features.destroy', 'uses' => 'FeatureController@destroy']);
	});

	Route::group(['prefix' => '/facilities/'], function ()
	{
		Route::get('/', ['as' => 'backend.facilities.index', 'uses' => 'FacilityController@index']);
		Route::get('/create', ['as' => 'backend.facilities.create.get', 'uses' => 'FacilityController@getCreate']);
		Route::post('/create', ['as' => 'backend.facilities.create.post', 'uses' => 'FacilityController@postCreate']);
		Route::get('/{id}', ['as' => 'backend.facilities.show', 'uses' => 'FacilityController@show']);
		Route::get('/{id}/edit', ['as' => 'backend.facilities.edit.get', 'uses' => 'FacilityController@getEdit']);
		Route::post('/{id}/edit', ['as' => 'backend.facilities.edit.post', 'uses' => 'FacilityController@postEdit']);
		Route::get('/{id}/set-image', ['as' => 'backend.facilities.set_image.get', 'uses' => 'FacilityController@getSetImage']);
		Route::post('/{id}/set-image', ['as' => 'backend.facilities.set_image.post', 'uses' => 'FacilityController@postSetImage']);
		Route::get('/{id}/destroy', ['as' => 'backend.facilities.destroy', 'uses' => 'FacilityController@destroy']);
	});

	Route::group(['prefix' => '/users/'], function ()
	{
		Route::get('/', ['as' => 'backend.users.index', 'uses' => 'UserController@index']);
		Route::get('/create', ['as' => 'backend.users.create.get', 'uses' => 'UserController@getCreate']);
		Route::post('/create', ['as' => 'backend.users.create.post', 'uses' => 'UserController@postCreate']);
		Route::get('/{id}', ['as' => 'backend.users.show', 'uses' => 'UserController@show']);
		Route::get('/{id}/edit', ['as' => 'backend.users.edit.get', 'uses' => 'UserController@getEdit']);
		Route::post('/{id}/edit', ['as' => 'backend.users.edit.post', 'uses' => 'UserController@postEdit']);
		Route::get('/{id}/change-password', ['as' => 'backend.users.change_password.get', 'uses' => 'UserController@getChangePassword']);
		Route::post('/{id}/change-password', ['as' => 'backend.users.change_password.post', 'uses' => 'UserController@postChangePassword']);
		Route::get('/{id}/destroy', ['as' => 'backend.users.destroy', 'uses' => 'UserController@destroy']);
	});

	Route::group(['prefix' => '/settings/'], function ()
	{
		Route::get('/', ['as' => 'backend.settings.index', 'uses' => 'SettingController@index']);
//		Route::get('/create', ['as' => 'backend.settings.create.get', 'uses' => 'SettingController@getCreate']);
//		Route::post('/create', ['as' => 'backend.settings.create.post', 'uses' => 'SettingController@postCreate']);
		Route::get('/{id}', ['as' => 'backend.settings.show', 'uses' => 'SettingController@show']);
		Route::get('/{id}/edit', ['as' => 'backend.settings.edit.get', 'uses' => 'SettingController@getEdit']);
		Route::post('/{id}/edit', ['as' => 'backend.settings.edit.post', 'uses' => 'SettingController@postEdit']);
//		Route::get('/{id}/destroy', ['as' => 'backend.settings.destroy', 'uses' => 'SettingController@destroy']);
	});

	Route::group(['prefix' => '/login-attempts/'], function ()
	{
		Route::get('/', ['as' => 'backend.login_attempts.index', 'uses' => 'LoginAttemptController@index']);
		Route::get('/timeline', ['as' => 'backend.login_attempts.timeline', 'uses' => 'LoginAttemptController@timeline']);
//		Route::get('/timeline', ['as' => 'backend.login_attempts.timeline', 'uses' => 'LoginAttemptController@timeline']);
		Route::get('/{id}', ['as' => 'backend.login_attempts.show', 'uses' => 'LoginAttemptController@show']);
//		Route::get('/{id}/edit', ['as' => 'backend.login_attempts.edit.get', 'uses' => 'LoginAttemptController@getEdit']);
//		Route::post('/{id}/edit', ['as' => 'backend.login_attempts.edit.post', 'uses' => 'LoginAttemptController@postEdit']);
	});

	Route::group(['prefix' => '/analytics/'], function ()
	{
		Route::group(['prefix' => '/facilities/'], function ()
		{
			Route::get('/', ['as' => 'backend.analytics.facilities.timeline', 'uses' => 'AnalyticsController@facilityClickTimeline']);
			Route::get('/timeline', ['as' => 'backend.analytics.facilities.timeline', 'uses' => 'AnalyticsController@facilityClickTimeline']);
			Route::get('/monthly', ['as' => 'backend.analytics.facilities.monthly', 'uses' => 'AnalyticsController@facilityClickMonthly']);
		});

		Route::group(['prefix' => '/features/'], function ()
		{
			Route::get('/', ['as' => 'backend.analytics.features.timeline', 'uses' => 'AnalyticsController@FeatureSearchTimeline']);
			Route::get('/timeline', ['as' => 'backend.analytics.features.timeline', 'uses' => 'AnalyticsController@FeatureSearchTimeline']);
			Route::get('/monthly', ['as' => 'backend.analytics.features.monthly', 'uses' => 'AnalyticsController@FeatureSearchMonthly']);
		});
	});
});