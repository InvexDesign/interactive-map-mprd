<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		/* If there was an error during "up()", the migration will not have completed
		and will NOT run a "down()" before running "up()" which will error out
		since each table before the error will already exist.
		Use this to clear all tables beforehand.
		*/
		$this->down();

		Schema::create('password_resets', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('email')->index();
			$table->string('token')->index();
			$table->timestamp('created_at')->nullable();
		});

		Schema::create('users', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('username')->unique();
			$table->string('email');
			$table->string('first_name')->nullable();
			$table->string('last_name')->nullable();
			$table->string('password');
			$table->dateTime('password_updated_at')->nullable();
			$table->boolean('is_active')->default(true);
			$table->rememberToken();
			$table->nullableTimestamps();
		});

		Schema::create('settings', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('key')->unique();
			$table->text('value')->nullable();
			$table->string('type', 25)->default('string');
			$table->text('description')->nullable();
			$table->integer('user_id')->unsigned()->nullable();
			$table->foreign('user_id')->references('id')->on('users');
			$table->nullableTimestamps();
		});

		Schema::create('states', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('short_name');
			$table->nullableTimestamps();
		});

		Schema::create('features', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('uid')->nullable();
			$table->string('name')->nullable();
			$table->string('tagline')->nullable();
			$table->text('description')->nullable();
			$table->string('image')->nullable();
			$table->integer('order')->nullable();
			$table->text('notes')->nullable();
			$table->nullableTimestamps();
		});

		Schema::create('facilities', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('uid')->nullable();
			$table->string('name')->nullable();
			$table->string('address1')->nullable();
			$table->string('address2')->nullable()->default(null);
			$table->string('city')->nullable();
			$table->integer('state_id')->unsigned();
			$table->foreign('state_id')->references('id')->on('states');
			$table->string('zipcode')->nullable();
			$table->boolean('auto_gps')->default(false);
			$table->decimal('latitude', 10, 7)->nullable();
			$table->decimal('longitude', 10, 7)->nullable();
			$table->string('url')->nullable();
			$table->string('image')->nullable();
			$table->string('phone')->nullable();
			$table->integer('order')->nullable();
			$table->text('description')->nullable();
			$table->text('notes')->nullable();
			$table->nullableTimestamps();
		});

		Schema::create('facility_feature', function (Blueprint $table)
		{
			$table->increments('id')->unsigned()->index();
			$table->integer('facility_id')->unsigned();
			$table->foreign('facility_id')->references('id')->on('facilities')->onDelete('cascade');
			$table->integer('feature_id')->unsigned();
			$table->foreign('feature_id')->references('id')->on('features')->onDelete('cascade');
			$table->nullableTimestamps();
		});

		Schema::create('login_attempts', function (Blueprint $table)
		{
			$table->increments('id')->unsigned()->index();
			$table->string('username')->nullable();
			$table->string('ip_address')->nullable();
			$table->dateTime('timestamp');
			$table->boolean('success')->nullable();
			$table->string('country')->nullable();
			$table->string('region')->nullable();
			$table->string('city')->nullable();
			$table->string('postal')->nullable();
			$table->decimal('latitude', 10, 7)->nullable();
			$table->decimal('longitude', 10, 7)->nullable();
			$table->string('organization')->nullable();
			$table->string('hostname')->nullable();
			$table->string('browser')->nullable();
			$table->string('referrer')->nullable();
			$table->integer('user_id')->unsigned()->nullable();
			$table->nullableTimestamps();
		});

		Schema::create('analytics__monthly_feature_searches', function (Blueprint $table)
		{
			$table->increments('id')->unsigned()->index();
			$table->integer('feature_id')->unsigned();
			$table->foreign('feature_id')->references('id')->on('features')->onDelete('cascade');
			$table->dateTime('month')->nullable();
			$table->bigInteger('hits')->unsigned();
			$table->nullableTimestamps();
		});

		Schema::create('analytics__monthly_facility_clicks', function (Blueprint $table)
		{
			$table->increments('id')->unsigned()->index();
			$table->integer('facility_id')->unsigned();
			$table->foreign('facility_id')->references('id')->on('facilities')->onDelete('cascade');
			$table->dateTime('month')->nullable();
			$table->bigInteger('hits')->unsigned();
			$table->nullableTimestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');

		Schema::dropIfExists('analytics__monthly_facility_clicks');
		Schema::dropIfExists('analytics__monthly_feature_searches');
		Schema::dropIfExists('login_attempts');
		Schema::dropIfExists('facility_feature');
		Schema::dropIfExists('facilities');
		Schema::dropIfExists('features');
		Schema::dropIfExists('states');
		Schema::dropIfExists('settings');
		Schema::dropIfExists('users');
		Schema::dropIfExists('password_resets');

		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
