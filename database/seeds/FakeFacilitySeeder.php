<?php

use App\Models\Facility;
use App\Models\Feature;
use App\Models\State;
use Illuminate\Database\Seeder;

class FakeFacilitySeeder extends Seeder
{
	public function run()
	{
		$faker = \Faker\Factory::create();
		$names = [
			'Backalley Burgers',
			'Burger Town',
			'Ketchup Jazz Fest',
			'Lifehouse Special',
			'Barn Salad Stew',
			'Togepi Surprise',
			'Slimers Den',
			'Poppy Seed Field',
			'Time Travel Spaceship',
			'Long Time Friend Zone',
			'Piecemeal Jazz Session',
		];
		$line2_options = [
			'Basement',
			'Building A',
			'Building B',
			'Building C',
			'Building D',
			'Classroom A',
			'Classroom B',
			'Classroom C',
			'Classroom D',
			'Main Gym',
		];
		$state_ids = State::query()->pluck('id')->all();
		$all_service_ids = Feature::all()->pluck('id')->all();
		$services_count = count($all_service_ids);
		$type_options = array_keys(Facility::getTypeOptions());

		$initial_latitude = Setting::get('initial-map-latitude', 41.8858001);
		$initial_longitude = Setting::get('initial-map-longitude', -88.326725);

		foreach($names as $i => $name)
		{
			$facility = Facility::create([
				'uid'               => 'FAC-' . ($i + 1),
				'name'              => $name,
				'tagline'           => $faker->sentence(2),
				'organization'      => $faker->sentence(2),
				'address1'          => $faker->streetAddress,
				'address2'          => $faker->boolean() ? null : $faker->randomElement($line2_options),
				'city'              => $faker->city,
				'state_id'          => $faker->randomElement($state_ids),
				'zipcode'           => $faker->numberBetween(10000, 99999),
				'auto_gps'          => true,
				'latitude'          => $initial_latitude + ($faker->numberBetween(100, 99999) / 1000000),
				'longitude'         => $initial_longitude + ($faker->numberBetween(100, 99999) / 1000000),
				'type'              => $faker->randomElement($type_options),
				'url'               => null,
				'email'             => null,
				'website'           => null,
				'phone'             => self::generateRandomPhoneNumber($faker),
				'hours'             => null,
				'image'             => null,
				'description'       => $faker->sentence(),
				'parking_transport' => null,
				'lgbtq_services'    => null,
				'minor_access'      => null,
				'appointments'      => null,
				'contact'           => null,
				'order'             => null,
				'notes'             => null,
			]);

			$service_ids = $faker->randomElements($all_service_ids, $faker->numberBetween(3, $services_count));
			$facility->services()->sync($service_ids);
		}
	}

	private static function generateRandomPhoneNumber($faker)
	{
		if($faker->boolean())
		{
			return $faker->numberBetween(200, 999) . $faker->numberBetween(200, 999) . $faker->numberBetween(2000, 9999);
		}

		return $faker->numberBetween(200, 999) . $faker->numberBetween(200, 999) . $faker->numberBetween(2000, 9999) . $faker->numberBetween(10, 999);
	}
}
