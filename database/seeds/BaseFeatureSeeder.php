<?php

use App\Models\Instructor;
use App\Models\Feature;
use App\Models\Statuses\Instructors\InstructorActivityStatus;
use Illuminate\Database\Seeder;

class BaseFeatureSeeder extends Seeder
{
	public function run()
	{
		$faker = \Faker\Factory::create();

		$features = [
			'Ball Fields',
			'Basketball Courts',
			'Drinking Fountains',
			'Playground',
			'Parking',
			'Picnic Area',
			'Shelters And Rentals',
			'Tennis Courts',
			'Boating',
			'Fishing',
			'Sled Hill',
			'Sand Volleyball Courts',
			'Disc Golf',
			'Bocce Court',
			'Shuffle Board',
			'Walking Path',
			'Outdoor Ice Skating',
			'Beach',
			'Golf',
			'Horseshoes',
			'Skate Park',
			'Biking',
		];

		foreach($features as $index => $name)
		{
			Feature::create([
				'uid'         => 'FEAT - ' . ($index + 1),
				'name'        => $name,
				'tagline'     => null,
				'description' => null,
				'image'       => null,
				'order'       => null,
				'notes'       => $faker->sentence(),
			]);
		}
	}
}
