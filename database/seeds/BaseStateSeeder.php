<?php

use App\Models\State;
use Illuminate\Database\Seeder;

class BaseStateSeeder extends Seeder
{
	public function run()
	{
		$state = State::create([
			'name'       => 'Alabama',
			'short_name' => 'AL'
		]);
		$state = State::create([
			'name'       => 'Alaska',
			'short_name' => 'AK'
		]);
		$state = State::create([
			'name'       => 'Arizona',
			'short_name' => 'AZ'
		]);
		$state = State::create([
			'name'       => 'Arkansas',
			'short_name' => 'AR'
		]);
		$state = State::create([
			'name'       => 'California',
			'short_name' => 'CA'
		]);
		$state = State::create([
			'name'       => 'Colorado',
			'short_name' => 'CO'
		]);
		$state = State::create([
			'name'       => 'Connecticut',
			'short_name' => 'CT'
		]);
		$state = State::create([
			'name'       => 'Delaware',
			'short_name' => 'DE'
		]);
		$state = State::create([
			'name'       => 'Florida',
			'short_name' => 'FL'
		]);
		$state = State::create([
			'name'       => 'Georgia',
			'short_name' => 'GA'
		]);
		$state = State::create([
			'name'       => 'Hawaii',
			'short_name' => 'HI'
		]);
		$state = State::create([
			'name'       => 'Idaho',
			'short_name' => 'ID'
		]);
		$state = State::create([
			'name'       => 'Illinois',
			'short_name' => 'IL'
		]);
		$state = State::create([
			'name'       => 'Indiana',
			'short_name' => 'IN'
		]);
		$state = State::create([
			'name'       => 'Iowa',
			'short_name' => 'IA'
		]);
		$state = State::create([
			'name'       => 'Kansas',
			'short_name' => 'KS'
		]);
		$state = State::create([
			'name'       => 'Kentucky',
			'short_name' => 'KY'
		]);
		$state = State::create([
			'name'       => 'Louisiana',
			'short_name' => 'LA'
		]);
		$state = State::create([
			'name'       => 'Maine',
			'short_name' => 'ME'
		]);
		$state = State::create([
			'name'       => 'Maryland',
			'short_name' => 'MD'
		]);
		$state = State::create([
			'name'       => 'Massachusetts',
			'short_name' => 'MA'
		]);
		$state = State::create([
			'name'       => 'Michigan',
			'short_name' => 'MI'
		]);
		$state = State::create([
			'name'       => 'Minnesota',
			'short_name' => 'MN'
		]);
		$state = State::create([
			'name'       => 'Mississippi',
			'short_name' => 'MS'
		]);
		$state = State::create([
			'name'       => 'Missouri',
			'short_name' => 'MO'
		]);
		$state = State::create([
			'name'       => 'Montana',
			'short_name' => 'MT'
		]);
		$state = State::create([
			'name'       => 'Nebraska',
			'short_name' => 'NE'
		]);
		$state = State::create([
			'name'       => 'Nevada',
			'short_name' => 'NV'
		]);
		$state = State::create([
			'name'       => 'New Hampshire',
			'short_name' => 'NH'
		]);
		$state = State::create([
			'name'       => 'New Jersey',
			'short_name' => 'NJ'
		]);
		$state = State::create([
			'name'       => 'New Mexico',
			'short_name' => 'NM'
		]);
		$state = State::create([
			'name'       => 'New York',
			'short_name' => 'NY'
		]);
		$state = State::create([
			'name'       => 'North Carolina',
			'short_name' => 'NC'
		]);
		$state = State::create([
			'name'       => 'North Dakota',
			'short_name' => 'ND'
		]);
		$state = State::create([
			'name'       => 'Ohio',
			'short_name' => 'OH'
		]);
		$state = State::create([
			'name'       => 'Oklahoma',
			'short_name' => 'OK'
		]);
		$state = State::create([
			'name'       => 'Oregon',
			'short_name' => 'OR'
		]);
		$state = State::create([
			'name'       => 'Pennsylvania',
			'short_name' => 'PA'
		]);
		$state = State::create([
			'name'       => 'Rhode Island',
			'short_name' => 'RI'
		]);
		$state = State::create([
			'name'       => 'South Carolina',
			'short_name' => 'SC'
		]);
		$state = State::create([
			'name'       => 'South Dakota',
			'short_name' => 'SD'
		]);
		$state = State::create([
			'name'       => 'Tennessee',
			'short_name' => 'TN'
		]);
		$state = State::create([
			'name'       => 'Texas',
			'short_name' => 'TX'
		]);
		$state = State::create([
			'name'       => 'Utah',
			'short_name' => 'UT'
		]);
		$state = State::create([
			'name'       => 'Vermont',
			'short_name' => 'VT'
		]);
		$state = State::create([
			'name'       => 'Virginia',
			'short_name' => 'VA'
		]);
		$state = State::create([
			'name'       => 'Washington',
			'short_name' => 'WA'
		]);
		$state = State::create([
			'name'       => 'West Virginia',
			'short_name' => 'WV'
		]);
		$state = State::create([
			'name'       => 'Wisconsin',
			'short_name' => 'WI'
		]);
		$state = State::create([
			'name'       => 'Wyoming',
			'short_name' => 'WY'
		]);
		$state = State::create([
			'name'       => 'Washington DC',
			'short_name' => 'DC'
		]);
	}
}
