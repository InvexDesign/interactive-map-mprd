<?php

use App\Models\Feature;
use Illuminate\Database\Seeder;

class FakeFeatureSeeder extends Seeder
{
	public function run()
	{
		$faker = \Faker\Factory::create();

		$services = [
			'Abortion Information',
			'Adoption Information',
			'Housing Referrals',
			'Maternity & Infant Supplies',
			'Medical Referrals',
			'Parenting Education',
			'Post-Abortion Support',
			'Pregnancy Options Information',
			'Pregnancy Tests',
			'STD/STI Information',
			'Ultrasound (Onsite)',
			'Ultrasound Referrals',
		];

		foreach($services as $index => $service)
		{
			Feature::create([
				'uid'         => 'SVC - ' . ($index + 1),
				'name'        => $service,
				'tagline'     => $faker->sentence(),
				'description' => $faker->sentence(),
				'image'       => null,
				'order'       => null,
				'notes'       => $faker->sentence(),
			]);
		}
	}

	private static function generateRandomPhoneNumber($faker)
	{
		if($faker->boolean())
		{
			return $faker->numberBetween(200, 999) . $faker->numberBetween(200, 999) . $faker->numberBetween(2000, 9999);
		}

		return $faker->numberBetween(200, 999) . $faker->numberBetween(200, 999) . $faker->numberBetween(2000, 9999) . $faker->numberBetween(10, 999);
	}
}
