<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Model::unguard();
		$is_clean_seed = config('system.is_clean_seed');

		$this->call('BaseRoleSeeder');
		$BASE_USER_SEEDER = env('BASE_USER_SEEDER', 'BaseUserSeeder');
		$this->call($BASE_USER_SEEDER);
		$this->call('BaseStateSeeder');

		$BASE_SETTING_SEEDER = env('BASE_SETTING_SEEDER', 'BaseSettingSeeder');
		$this->call($BASE_SETTING_SEEDER);

		$BASE_FEATURE_SEEDER = env('BASE_FEATURE_SEEDER', 'BaseFeatureSeeder');
		$this->call($BASE_FEATURE_SEEDER);
		$BASE_FACILITY_SEEDER = env('BASE_FACILITY_SEEDER', 'BaseFacilitySeeder');
		$this->call($BASE_FACILITY_SEEDER);

		if(!$is_clean_seed)
		{
			$FAKE_ANALYTICS_SEEDER = env('FAKE_ANALYTICS_SEEDER', 'FakeAnalyticsSeeder');
			$this->call($FAKE_ANALYTICS_SEEDER);
		}
    }
}
