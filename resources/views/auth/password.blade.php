@extends('templates.external')

@section('content')

    <h3>Reset Password</h3>

    {!! Form::open(['route' => 'password.reset.email.post']) !!}

        <div class="form-group">
            {!! Form::label('email', 'Email Address') !!}
            {!! Form::email('email', null, ['class'=>'form-control']) !!}
        </div>
        <br />
        <div class="flex-container form-group">
            {!! Form::submit('Send Password Reset Link', ['class' => 'btn btn-primary', 'style' => 'flex: 1;']) !!}
        </div>

    {!! Form::close() !!}

@stop