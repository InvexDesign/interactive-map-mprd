<div class="top">
    <h2 class="page-title">@yield('page-title')</h2>
    <ul class="controls">
        @if (Auth::guest())
            <li><a href="{{ url('/login') }}">Login</a></li>
        @else
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Options <span class="caret"></span></a>
                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                    <li><a href="{{ route('backend.my_account.show') }}">My Account</a></li>
                    <li><a href="{{ route('session.destroy') }}">Logout</a></li>
                </ul>
            </li>
        @endif
    </ul>
</div>
@if (View::hasSection('breadcrumbs'))
    <div class="bottom">
        <div class="breadcrumb-wrapper">
            <span class="title">You are here:</span>
            @yield('breadcrumbs')
        </div>
    </div>
@endif

