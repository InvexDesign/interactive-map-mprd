<?php
$nav = [];

$nav[] = $inventory = [
        '#'        => 'Inventory',
        'sub-menu' => [
                route('skids.index')  => 'View Inventory',
                route('skids.create') => 'Create Skid (Legacy)',

        ],
        'url-map'  => [
                'skids'
        ]
];
$nav[] = $inventory = [
        '#'        => 'Production Orders',
        'sub-menu' => [
                route('production_orders.index')  => 'View Production Orders',
                route('production_orders.create') => 'Create Production Order'
        ],
        'url-map'  => [
                'production-orders'
        ]
];
$nav[] = $purchase_orders = [
        '#'        => 'Purchase Orders',
        'sub-menu' => [
                route('purchase_orders.index')      => 'View Purchase Orders',
                route('purchase_orders.create')     => 'Create Purchase Order',
                '',
                route('purchase_order_items.index') => 'View Purchase Order Items'
        ],
        'url-map'  => [
                'purchase-orders',
                'purchase-order-items'
        ]
];
$nav[] = $sale_orders = [
        '#'        => 'Sale Orders',
        'sub-menu' => [
                route('sale_orders.index')                                           => 'View Sale Orders',
                route('sale_orders.create')                                          => 'Create Sale Order',
                '',
                route('sale_orders.create.template', ['template' => 'McMasterCarr']) => 'Create McMaster Carr Sale Order',
                route('sale_orders.create.template', ['template' => 'Revcor'])       => 'Create Revcor Sale Order',
                '',
                route('sale_order_items.index')                                      => 'View Sale Order Items'
        ],
        'url-map'  => [
                'sale-orders',
                'sale-order-items'
        ]
];
$nav[] = $address_book = [
        '#'        => 'Address Book',
        'sub-menu' => [
                route('companies.index')  => 'View Companies',
                route('companies.create') => 'Create Company',
                '',
                route('contacts.index')   => 'View Contacts',
                route('contacts.create')  => 'Create Contact',
                '',
                route('addresses.index')  => 'View Addresses',
                route('addresses.create') => 'Create Address'
        ],
        'url-map'  => [
                'companies',
                'contacts',
                'addresses'
        ]
];
$nav[] = $attachments = [
        route('attachments.index') => 'Attachments',
        'url-map'                  => [
                'attachments'
        ]
];
$nav[] = $analytics = [
        '#'        => 'Analytics',
        'sub-menu' => [
                route('analytics.summary')  => 'Summary',
                '',
                route('analytics.sales.summary')  => 'Sale Summary',
                route('analytics.sales.items')  => 'By Item',
                route('analytics.sales.customers')  => 'By Customer',
                route('analytics.sales.skid_types')  => 'By Skid Type',
                '',
                route('analytics.purchases.summary')  => 'Purchase Summary',
                route('analytics.purchases.items')  => 'By Item',
                route('analytics.purchases.vendors')  => 'By Vendor',
                route('analytics.purchases.skid_types')  => 'By Skid Type',
        ],
        'url-map'  => [
                'analytics',
        ]
];
$nav[] = $users = [
        '#'        => 'Users',
        'sub-menu' => [
                route('users.index')  => 'View Users',
                route('users.create') => 'Create User'
        ],
        'url-map'  => [
                'users'
        ]
];
$nav[] = $settings = [
        '#'        => 'Settings',
        'sub-menu' => [
                route('settings.index')    => 'Manage System Settings',
                '',
                route('gauges.index')      => 'View Gauges',
                route('gauges.create')     => 'Create Gauge',
                '',
                route('skid_types.index')  => 'View Skid Types',
                route('skid_types.create') => 'Create Skid Type'
        ],
        'url-map'  => [
                'settings',
                'gauges',
                'skid-types'
        ]
];
?>
<ul class="static-navigation sidebar-nav">
    <li class="{{ count(Request::segments()) < 1 ? 'active' : '' }}"><a  class="{{ count(Request::segments()) < 1 ? 'active' : '' }}" href="/">Home</a></li>
    @foreach($nav as $section)
        <?php
        $current_url = Request::url();
        $is_current = false;
        //if(isset($section[$url]) || (isset($section['sub-menu']) && isset($section['sub-menu'][$url])))//in_array($url, array_keys($section['sub-menu']))) )
        if(count(Request::segments()) > 0)
        {
            $url_prefix = Request::segments()[0];
            if(in_array($url_prefix, $section['url-map']))
            {
                $is_current = true;
            }
        }
        ?>

        @if( isset($section['sub-menu']) )
            <li class="dropdown {{ $is_current ? 'active' : '' }}">
                <a href="#" data-toggle="dropdown"
                   class="dropdown-toggle {{ $is_current ? 'active' : '' }}">{{ $section['#'] }} <b
                            class="caret"></b></a>
                {{--<ul class="static-dropdown-menu {{ $is_current ? 'active' : '' }}">--}}
                @if($is_current)
                    <ul class="static-dropdown-menu" style="display: block;">
                        @else
                            <ul class="static-dropdown-menu">
                                @endif
                                @foreach($section['sub-menu'] as $url => $display)
                                    @if($display == '')
                                        <li class="divider">-----------</li>
                                    @else
                                        @if($current_url == $url)
                                            <li class="active"><a class="active" href="{{ $url }}">{{ $display }}</a>
                                            </li>
                                        @else
                                            <li><a href="{{ $url }}">{{ $display }}</a></li>
                                        @endif
                                    @endif
                                @endforeach
                            </ul>
            </li>
        @else
            <?php list($url, $display) = each($section); ?>
            <li class="{{ $is_current ? 'active' : '' }}"><a class="{{ $is_current ? 'active' : '' }}"
                                                             href="{{ $url }}">{{ $display }}</a></li>
        @endif
    @endforeach
</ul>
<div class="clearer"></div>