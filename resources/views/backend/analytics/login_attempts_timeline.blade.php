@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Login Attempts Timeline')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Analytics'         => null,
        'Login Attempts'    => null,
        'Timeline'          => null
    ]); !!}
@stop

@section('content')

    <h4 style="text-align: center;">Date Range: {{ $start_at->format('m/d/Y') }} - {{ $end_at->format('m/d/Y') }}</h4>
    <canvas id="chart" width="800" height="450"></canvas>

    <script>
        $(document).ready(function()
        {
            new Chart(document.getElementById("chart"), {
                type: 'bar',
                data: {
                    labels: ['{!! implode("','", $data['labels']) !!}'],
                    datasets: [
                        @foreach($data['datasets'] as $dataset)
                        {
                            label: '{{ $dataset['label'] }}',
                            borderColor: '{{ $dataset['color'] }}',
                            backgroundColor: '{{ $dataset['color'] }}',
                            fill: '{{ $dataset['fill'] }}',
                            data: [{{ implode(',', $dataset['data']) }}],
                        },
                        @endforeach
                    ]
                },
                options: {
                    title: {
                        display: true,
                        text: '{{ $title }}'
                    }
                }
            });
        });
    </script>
@stop

