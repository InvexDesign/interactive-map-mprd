@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Edit User')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Users'         => ['backend.users.index'],
        $user->username => ['backend.users.show', $user->id],
        'Edit'          => null,
    ]); !!}
@stop

@section('content')
    {!! Form::model($user, ['route' => ['backend.users.edit.post', $user->id]]) !!}

        @include('backend.users.partials.form')

        {!! Form::submit('Update User', ['class' => 'btn btn-primary']) !!}

    {!! Form::close() !!}
@stop


