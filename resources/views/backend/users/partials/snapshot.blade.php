<p>
    <strong>Username:</strong> {{ $user->username }}<br />
    <strong>Email:</strong> <a href="mailto:{{ $user->email }}">{{ $user->email }}</a><br />
    <strong>Name:</strong> {{ $user->first_last }}<br />
    <strong>Role:</strong> {{ $user->getRole()->display_name }}<br />
</p>