<div class="col-xs-12 form-group">
    {!! Form::label('kitchen_ids[]', 'Kitchens *') !!}
    {!! Form::select('kitchen_ids[]', $kitchen_options, isset($kitchen_ids) ? $kitchen_ids : null, ['multiple' => true, 'class'=>'select-two form-control']) !!}
    {!! $errors->first('kitchen_ids[]', '<span class="error">:message</span>') !!}
</div>
<div class="clearer"></div>