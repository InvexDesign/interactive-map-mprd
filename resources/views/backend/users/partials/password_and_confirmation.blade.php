<div class="field form-group">
    {!! Form::label('password', 'Password *') !!}
    {!! Form::password('password', ['class'=>'form-control']) !!}
</div>
<div class="field form-group">
    {!! Form::label('password_confirmation', 'Password Confirmation *') !!}
    {!! Form::password('password_confirmation', ['class'=>'form-control']) !!}
</div>
<div class="clearer"></div>
