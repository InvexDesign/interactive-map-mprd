@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Set Image')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Facilities'        => ['backend.facilities.index'],
        '#' . $facility->id => ['backend.facilities.show', $facility->id],
        'Set Image'         => null,
    ]); !!}
@stop

@section('content')

    {!! Form::open(['route' => ['backend.facilities.set_image.get', $facility->id], 'files' => 'true']) !!}

    @include('includes.image_input', ['label' => 'Image *', 'name' => 'image'])

    <div class="form-group">
        {!! Form::submit('Set Image', ['class' => 'btn btn-primary']) !!}
    </div>

    {!! Form::close() !!}

@stop