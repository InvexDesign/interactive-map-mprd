@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'View Facility')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Facilities'         => ['backend.facilities.index'],
        '#' . $facility->id  => null,
    ]); !!}
@stop

@section('content')
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#facility">Facility</a></li>
        <li><a data-toggle="tab" href="#features">Features</a></li>
    </ul>

    <div class="tab-content">
        <div id="facility" class="tab-pane fade in active">
            <br />
            {!! link_to_route('backend.facilities.set_image.get', 'Set Image', ['id' => $facility->id], ['class' => 'btn btn-success']) !!}
            {!! link_to_route('backend.facilities.edit.get', 'Edit Facility', ['id' => $facility->id], ['class' => 'btn btn-primary']) !!}
            {!! link_to_route('backend.facilities.destroy', 'Delete Facility', ['id' => $facility->id], ['class' => 'btn btn-danger']) !!}
            <br />
            <br />


            <h4>Current Image</h4>
            <img src="{{ $facility->getImageUrl() }}" width="250" />
            <br />
            <br />

            @include('backend.facilities.partials.snapshot')
        </div>
        <div id="features" class="tab-pane fade">
            @include('backend.features.partials.table', ['features' => $facility->features])
        </div>
    </div>
@stop