<p>
    <strong>Name:</strong> {{ $facility->name }}<br />
    <strong>Address 1:</strong> {{ $facility->address1 }}<br />
@if($facility->address2)
    <strong>Address 2:</strong> {{ $facility->address2 }}<br />
@endif
    <strong>City:</strong> {{ $facility->city }}<br />
    <strong>State:</strong> {{ $facility->state->display() }}<br />
    <strong>Zip Code:</strong> {{ $facility->zipcode }}<br />
    <strong>Auto GPS:</strong> {{ $facility->auto_gps }}<br />
    <strong>Latitude:</strong> {{ $facility->latitude }}<br />
    <strong>Longitude:</strong> {{ $facility->longitude }}<br />
    <strong>URL:</strong> <a href="{{ $facility->url }}" target="_blank">{{ $facility->url }}</a><br />
    <strong>Image:</strong> {{ $facility->image }}<br />
    <strong>Phone:</strong> {{ $facility->phone }}<br />
    <strong>Order:</strong> {{ $facility->order }}<br />
    <strong>Description:</strong> {{ $facility->description }}<br />
    <strong>Notes:</strong> {{ $facility->notes }}<br />
</p>