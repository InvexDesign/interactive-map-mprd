<?php $table_id = isset($table_id) ? $table_id : 'facility_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>UID</th>
        <th>Name</th>
        <th>Address</th>
        <th>Latitude</th>
        <th>Longitude</th>
        <th>URL</th>
        <th>Phone</th>
        <th>Description</th>
        <th>Notes</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody>
    @foreach($facilities as $facility)
        <tr>
            <td>{!! link_to_route('backend.facilities.show', $facility->id, ['id' => $facility->id]) !!}</td>
            <td>{!! link_to_route('backend.facilities.show', $facility->uid, ['id' => $facility->id]) !!}</td>
            <td>{{ $facility->name }}</td>
            <td>{{ $facility->displayAddress(true) }}</td>
            <td>{{ $facility->latitude }}</td>
            <td>{{ $facility->longitude }}</td>
            <td>{{ $facility->url }}</td>
            <td>{{ $facility->phone }}</td>
            <td>{{ $facility->description }}</td>
            <td>{{ $facility->notes }}</td>
            <td class="icon-td">
                {!! Helper::icon_to_route('backend.facilities.show', 'fa-eye', [$facility->id], ['title' => 'View Facility']) !!}
                {!! Helper::icon_to_route('backend.facilities.edit.get', 'fa-pencil', [$facility->id], ['title' => 'Edit Facility']) !!}
                {!! Helper::icon_to_route('backend.facilities.destroy', 'fa-ban', [$facility->id], ['title' => 'Delete Facility']) !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<script>
    $(document).ready(function()
    {

        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => ['UID' => 1,'Latitude' => 1,'Longitude' => 1,'Description' => 1,'Notes' => 1]])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {hiddenColumns: hidden_columns});
    });
</script>