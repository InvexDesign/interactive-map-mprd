{{-- TODO: image --}}
<div class="field-container col-3">
    <div class="field form-group">
        {!! Form::label('name', 'Name *') !!}
        {!! Form::text('name', null, ['class'=>'form-control']) !!}
    </div>
@if(isset($allow_image) && $allow_image)
    <div class="field full-width js">
        @include('includes.image_input', ['label' => 'Image *', 'name' => 'image'])
    </div>
@endif
    <div class="field full-width form-group">
        {!! Form::label('description', 'Description') !!}
        {!! Form::textarea('description', null, ['class'=>'cazary form-control']) !!}
    </div>
    <div class="field half-width form-group">
        {!! Form::label('address1', 'Address Line 1 *') !!}
        {!! Form::text('address1', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field half-width form-group">
        {!! Form::label('address2', 'Address Line 2') !!}
        {!! Form::text('address2', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('city', 'City *') !!}
        {!! Form::text('city', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('state_id', 'State *') !!}
        {!! Form::select('state_id', $state_options, null, ['class'=>'select-two form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('zipcode', 'Zipcode *') !!}
        {!! Form::text('zipcode', isset($zipcode) ? $zipcode : null, [isset($lock_zipcode) && $lock_zipcode ? 'readonly' : '', 'class'=>'form-control']) !!}
    </div>
    <div class="field full-width form-group">
        {!! Form::label('auto_gps', 'Auto GPS') !!}
        {!! Form::checkbox('auto_gps', 1, isset($auto_gps) ? $auto_gps : true, ['class'=>'form-control']) !!}
    </div>
    <div class="field half-width form-group lat-long">
        {!! Form::label('latitude', 'Latitude *') !!}
        {!! Form::text('latitude', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field half-width form-group lat-long">
        {!! Form::label('longitude', 'Longitude *') !!}
        {!! Form::text('longitude', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field full-width form-group">
        {!! Form::label('feature_ids[]', 'Services') !!}
        {!! Form::select('feature_ids[]', $feature_options, isset($initial_feature_ids) ? $initial_feature_ids : null, ['multiple' => true, 'class'=>'select-two updatable form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('url', 'URL') !!}
        {!! Form::text('url', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('phone', 'Phone') !!}
        {!! Form::text('phone', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field full-width form-group">
        {!! Form::label('notes', 'Notes') !!}
        {!! Form::textarea('notes', null, ['class'=>'form-control']) !!}
    </div>
</div>
<script>
    $(document).ready(function()
    {
        $('#auto_gps').change(function()
        {
            $('.lat-long').toggle(!$(this).is(":checked"));
        }).trigger('change');
    });
</script>