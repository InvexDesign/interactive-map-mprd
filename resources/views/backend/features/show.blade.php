@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'View Feature')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Features'            => ['backend.features.index'],
        $feature->display()   =>  null
    ]); !!}
@stop

@section('content')
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#feature">Feature</a></li>
        <li><a data-toggle="tab" href="#facilities">Facilities</a></li>
    </ul>

    <div class="tab-content">
        <div id="feature" class="tab-pane fade in active">
            {!! link_to_route('backend.features.edit.get', 'Edit Feature', ['id' => $feature->id], ['class' => 'btn btn-primary']) !!}
            {!! link_to_route('backend.features.destroy', 'Delete Feature', ['id' => $feature->id], ['class' => 'btn btn-danger']) !!}
            <br />
            <br />
            @include('backend.features.partials.snapshot')
        </div>
        <div id="facilities" class="tab-pane fade">
            @include('backend.facilities.partials.table', ['facilities' => $feature->facilities])
        </div>
    </div>
@stop