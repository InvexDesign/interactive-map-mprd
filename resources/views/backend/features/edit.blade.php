@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Edit Feature')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Features'            => ['backend.features.index'],
        '#' . $feature->id    => ['backend.features.show', $feature->id],
        'Edit'                =>  null
    ]); !!}
@stop

@section('content')

    {!! Form::model($feature, ['route' => ['backend.features.edit.post', $feature->id]]) !!}

        @include('backend.features.partials.form')

        <div class="form-group">
            {!! Form::submit('Update Feature', ['class' => 'updatable btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@stop