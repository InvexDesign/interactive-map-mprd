@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Create Feature')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
            'Features' => ['backend.features.index'],
            'Create'   =>  null
    ]); !!}
@stop

@section('content')
    {!! Form::open(['route' => 'backend.features.create.post']) !!}

        @include('backend.features.partials.form')

        <div class="form-group">
            {!! Form::submit('Create Feature', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}
@stop