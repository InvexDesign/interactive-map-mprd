<p>
    <strong>ID:</strong> {{ $feature->id }}<br />
    <strong>UID:</strong> {{ $feature->uid }}<br />
    <strong>Name:</strong> {{ $feature->name }}<br />
    <strong>Tagline:</strong> {{ $feature->tagline }}<br />
    <strong>Description:</strong> {{ $feature->description }}<br />
    <strong>Notes:</strong> {{ $feature->notes }}<br />
</p>