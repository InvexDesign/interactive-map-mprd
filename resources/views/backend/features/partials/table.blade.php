<?php $table_id = isset($table_id) ? $table_id : 'feature_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>UID</th>
        <th>Name</th>
        <th>Tagline</th>
        <th>Description</th>
        <th>Notes</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody>
    @foreach($features as $feature)
        <tr>
            <td>{!! link_to_route('backend.features.show', $feature->id, [$feature->id]) !!}</td>
            <td>{!! link_to_route('backend.features.show', $feature->uid, [$feature->id]) !!}</td>
            <td>{!! link_to_route('backend.features.show', $feature->display(), [$feature->id]) !!}</td>
            <td>{{ $feature->tagline }}</td>
            <td>{{ $feature->description }}</td>
            <td>{{ $feature->notes }}</td>
            <td class="icon-td">
                {!! Helper::icon_to_route('backend.features.show', 'fa-eye', [$feature->id], ['title' => 'View Feature']) !!}
                {!! Helper::icon_to_route('backend.features.edit.get', 'fa-pencil', [$feature->id], ['title' => 'Edit Feature']) !!}
                {!! Helper::icon_to_route('backend.features.destroy', 'fa-ban', [$feature->id], ['title' => 'Delete Feature']) !!}
            </td>
        </tr>
    @endforeach
    </tbody>

</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => ['ID' => 1,'UID' => 1,'Tagline' => 1,'Description' => 1,'Notes' => 1]])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {hiddenColumns: hidden_columns});

        @if(isset($has_filtering) && $has_filtering)
        $('.filter').change(function()
        {
            datatables[table_id].draw();
        });
        @endif
    });
</script>