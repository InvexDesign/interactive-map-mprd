@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Features')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Features' => null,
    ]); !!}
@stop

@section('content')

    {!! link_to_route('backend.features.create.get', 'Create Feature', [], ['class' => 'btn btn-primary']) !!}
    <br />
    <br />

    @include('backend.features.partials.table')

@stop