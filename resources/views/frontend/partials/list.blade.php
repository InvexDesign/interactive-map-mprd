<ul class="marker-list">
    @foreach($facilities as $facility)
        @include('frontend.partials.list_item', compact('facility', 'APP_URL_BASE'))
    @endforeach
</ul>