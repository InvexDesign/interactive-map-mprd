<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/select2.full.min.js') }}"></script>
<script src="{{ asset('js/featherlight.min.js') }}"></script>
<script src="{{ asset('js/helpers.js') }}"></script>
<script src="{{ asset('js/utilities.js') }}"></script>

<script>
    function initializeInputs(scope)
    {
        if(typeof scope === 'undefined')
        {
            scope = '';
        }

        $('.lightbox-trigger').click(function()
        {
            var id = $(this).data('lightbox-id');
            $.featherlight('#' + id, {});
        });

        $(scope + '.select-two').select2();
        $(scope + '.select-two-tags').select2({
            tags: true,
            createTag: function (params) {
                return {
                    id: params.term,
                    text: params.term,
                    newOption: true
                }
            }
        });
    }

    $(document).ready(function()
    {
        initializeInputs();
    });
</script>