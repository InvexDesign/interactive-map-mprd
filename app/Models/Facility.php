<?php namespace App\Models;

use App\Interfaces\Displayable;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class Facility extends BaseModel implements Displayable, HasMedia
{
	use HasMediaTrait;

	protected $table    = 'facilities';
	protected $fillable = [
		'uid',
		'name',
		'address1',
		'address2',
		'city',
		'state_id',
		'zipcode',
		'auto_gps',
		'latitude',
		'longitude',
		'url',
		'image',
		'order',
		'phone',
		'description',
		'notes',
	];

	public static function getAttributeMap()
	{
		return [
			'name'          => 'string-escaped',
			'address1'      => 'string-escaped',
			'address2'      => 'string-escaped-nullable',
			'city'          => 'string-escaped',
			'zipcode'       => 'string-escaped',
			'used_auto_gps' => 'boolean',
			'latitude'      => 'float',
			'longitude'     => 'float',
			'url'           => 'string-escaped',
			'image'         => 'string-escaped',
			'order'         => 'integer',
			'phone'         => 'string-escaped',
			'description'   => 'text-escaped-nullable',
			'notes'         => 'text-escaped-nullable',
		];
	}


	/* ELOQUENT */
	public function state()
	{
		return $this->belongsTo(State::class, 'state_id');
	}

	public function features()
	{
		return $this->belongsToMany(Feature::class);
	}

	public function getDisplayAttribute()
	{
		return $this->display();
	}


	/* PRESENTATION */
	public function display()
	{
		return $this->name;
	}

	public function displayAddress($inline = false)
	{
		$separator = "<br />";
		if($inline)
		{
			$separator = " ";
		}

		$display = $this->address1 . $separator;

		if($this->address2)
		{
			$display .= $this->address2 . $separator;
		}

		$display .= $this->city . ", " . $this->state->display() . " " . $this->zipcode;

		return $display;
	}

	public function getImageUrl()
	{
		$media = $this->getMedia();
		if(count($media) == 0)
		{
			return '';
		}

		return $media[0]->getUrl();
	}


	/* ANALYTICS*/
	public function incrementClickAnalytics()
	{
		MonthlyFacilityClickRecord::incrementCurrentMonthsRecord($this);
	}
}