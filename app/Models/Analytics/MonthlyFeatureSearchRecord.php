<?php namespace App\Models;

use App\Interfaces\Displayable;
use Carbon;

class MonthlyFeatureSearchRecord extends BaseModel implements Displayable
{
	protected $table    = 'analytics__monthly_feature_searches';
	protected $fillable = [
		'feature_id',
		'month',
		'hits',
	];

	public static function getAttributeMap()
	{
		return [
			'month' => 'datetime',
			'hits'  => 'integer',
		];
	}

	public static function incrementCurrentMonthsRecord($feature)
	{
		$month = (new Carbon('first day of this month'))->startOfDay();
		$record = self::query()
					  ->where('feature_id', $feature->id)
					  ->where('month', $month)
					  ->get()
					  ->first();

		if(!$record)
		{
			self::create([
				'feature_id' => $feature->id,
				'month'       => $month,
				'hits'        => 1,
			]);
		}
		else
		{
			$record->hits = $record->hits + 1;
			$record->save();
		}
	}


	/* ELOQUENT */
	public function feature()
	{
		return $this->belongsTo(Feature::class, 'feature_id');
	}

	public function getDisplayAttribute()
	{
		return $this->display();
	}


	/* PRESENTATION */
	public function display()
	{
		return $this->month->format('m/d/Y') . ' | ' . $this->feature->display() . ': ' . $this->hits;
	}
}