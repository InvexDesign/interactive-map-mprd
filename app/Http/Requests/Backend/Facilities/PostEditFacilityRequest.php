<?php namespace App\Http\Requests\Backend\Facility;

use App\Http\Requests\BaseRequest;
use App\Models\Facility;
use HttpResponseException;
use Redirect;

class PostEditFacilityRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$facility = Facility::find($id);
		if(!$facility)
		{
			$redirect = Redirect::route('backend.facilities.index')->with('errors', ["Facility #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->_validate($this->all(), [
			'uid'         => '',
			'name'        => 'required',
			'address1'    => 'required',
			'address2'    => '',
			'city'        => 'required',
			'state_id'    => 'required|exists:states,id',
			'zipcode'     => 'required',
			'latitude'    => 'required_without:auto_gps',
			'longitude'   => 'required_without:auto_gps',
			'type'        => 'required',
			'url'         => '',
			'image'       => 'required',
			'phone'       => '',
			'order'       => '',
			'description' => '',
			'notes'       => '',
			'feature_ids' => 'required',
		]);

		$this->merge(['facility' => $facility]);
	}

	public function authorize()
	{
		return true;
	}
}
