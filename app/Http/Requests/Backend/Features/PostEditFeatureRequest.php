<?php namespace App\Http\Requests\Backend;

use App\Http\Requests\BaseRequest;
use App\Models\Feature;
use HttpResponseException;
use Redirect;

class PostEditFeatureRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$feature = Feature::find($id);
		if(!$feature)
		{
			$redirect = Redirect::route('backend.features.index')->with('errors', ["Feature #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->_validate($this->all(), [
			'uid'         => '',
			'name'        => 'required',
			'tagline'     => '',
			'description' => '',
			'image'       => '',
			'order'       => '',
			'notes'       => '',
		]);
		
		$this->merge(['feature' => $feature]);
	}

	public function authorize()
	{
		return true;
	}
}
