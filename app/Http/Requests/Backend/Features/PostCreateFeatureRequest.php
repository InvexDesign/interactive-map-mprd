<?php namespace App\Http\Requests\Backend;

use App\Http\Requests\BaseRequest;

class PostCreateFeatureRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$this->_validate($this->all(), [
			'uid'         => '',
			'name'        => 'required',
			'tagline'     => '',
			'description' => '',
			'image'       => '',
			'order'       => '',
			'notes'       => '',
		]);
	}

	public function authorize()
	{
		return true;
	}
}
