<?php namespace App\Http\Controllers;

use App\Models\Facility;
use App\Models\MonthlyFacilityClickRecord;
use App\Models\Feature;
use Exception;
use Input;

class FrontendController extends Controller
{
	public function interactiveMap()
	{
		$features = Feature::all();
		$feature_options = Feature::all()->pluck('name', 'id')->all();
		$facilities = Facility::all();

		$vars = compact(
			'features',
			'feature_options',
			'facilities'
		);

		return view('frontend.interactive_map', $vars);
	}

	public function filter()
	{
		try
		{
			$name = Input::get('name', false);
			$feature_ids = Input::get('feature_ids', []);
			$type = Input::get('type', false);

			$facilities_query = Facility::query();

			if($name !== false && $name !== '')
			{
				$facilities_query = $facilities_query->where('name', 'like', '%' . $name . '%');
			}

			if($type !== false && intval($type) != 0)
			{
				$facilities_query = $facilities_query->whereIn('type', [0, $type]);
			}

			if(count($feature_ids) > 0)
			{
				$facility_ids = Facility::query()->pluck('id')->all();
				$features = Feature::whereIn('id', $feature_ids)->get();
				foreach($features as $feature)
				{
					$feature->incrementSearchAnalytics();
					$facility_ids = array_intersect($facility_ids, $feature->facilities()->pluck('facility_id')->all());
				}

				$facilities_query = $facilities_query->whereIn('id', $facility_ids);
			}

			$success = true;
			$visible_facility_ids = $facilities_query->pluck('id')->all();
			$hidden_facility_ids = Facility::query()->whereNotIn('id', $visible_facility_ids)->pluck('id')->all();

			$facilities = [];
			foreach($visible_facility_ids as $id)
			{
				$facilities[$id] = true;
			}

			foreach($hidden_facility_ids as $id)
			{
				$facilities[$id] = false;
			}

			$json = compact('success','facilities');
		}
		catch(Exception $e)
		{
			$json = [
				'success' => false,
				'message' => $e->getMessage(),
				'line'    => $e->getLine(),
				'file'    => $e->getFile(),
				'trace'   => $e->getTraceAsString(),
			];
		}

		return json_encode($json);
	}

	public function processFacilityClick()
	{
//		try
//		{
			$facility_id = Input::get('facility_id', null);
			if($facility_id)
			{
				MonthlyFacilityClickRecord::incrementCurrentMonthsRecord($facility_id);
			}
//		}
//		catch(Exception $e)
//		{
//			\Log::error('processFacilityClick FAILED');
//			\Log::error($e->getMessage());
//		}
	}
}