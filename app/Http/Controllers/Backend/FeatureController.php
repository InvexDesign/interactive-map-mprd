<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\DestroyFeatureRequest;
use App\Http\Requests\Backend\PostCreateFeatureRequest;
use App\Http\Requests\Backend\PostEditFeatureRequest;
use App\Models\Feature;
use Illuminate\Support\Facades\Redirect;

class FeatureController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public static function composeViews()
	{
		view()->composer('backend.features.partials.form', function ($view)
		{
//			if(isset($view->getData()['feature']))
//			{
//				$feature = $view->getData()['feature'];
//			}
		});
	}

	public function index()
	{
		$features = Feature::query()->get();
		$vars = compact('features');

		return view('backend.features.index', $vars);
	}

	public function getCreate()
	{
		$vars = [];

		return view('backend.features.create', $vars);
	}

	public function postCreate(PostCreateFeatureRequest $request)
	{
		$feature = Feature::create($request->all());

		return Redirect::route('backend.features.show', $feature->id)->with('messages', ['Feature was successfully created!']);
	}

	public function show($id)
	{
		$feature = Feature::findOrFail($id);
		$vars = compact('feature');

		return view('backend.features.show', $vars);
	}

	public function getEdit($id)
	{
	    $feature = Feature::findOrFail($id);
	    $vars = compact('feature');
	    
	    return view('backend.features.edit', $vars);
	}

	public function postEdit(PostEditFeatureRequest $request, $id)
	{
		$feature = $request->feature;
		$feature->fill($request->all());
		$feature->save();

		return Redirect::route('backend.features.edit.get', $id)->with('messages', ['Feature was successfully updated!']);
	}

	public function destroy(DestroyFeatureRequest $request, $id)
	{
		$feature = $request->feature;
		$feature->delete();

		$message = "Feature #$id successfully deleted!";

		return Redirect::route('backend.features.index')->with('messages', [$message]);
	}
}