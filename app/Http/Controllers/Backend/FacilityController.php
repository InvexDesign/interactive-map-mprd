<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Facility\DestroyFacilityRequest;
use App\Http\Requests\Backend\Facility\PostCreateFacilityRequest;
use App\Http\Requests\Backend\Facility\PostEditFacilityRequest;
use App\Http\Requests\Backend\Facility\PostSetImageRequest;
use App\Models\Facility;
use App\Models\Feature;
use App\Models\State;
use Illuminate\Support\Facades\Redirect;

class FacilityController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public static function composeViews()
	{
		view()->composer('backend.facilities.partials.form', function ($view)
		{
			if(isset($view->getData()['facility']))
			{
				$facility = $view->getData()['facility'];
				$view->with('initial_feature_ids', $facility->features->pluck('id')->all());
			}

			$view->with('feature_options', Feature::all()->pluck('name', 'id')->all());
			$view->with('state_options', State::all()->pluck('short_name', 'id')->all());
		});
	}

	public function index()
	{
		$facilities = Facility::all();
		$vars = compact('facilities');

		return view('backend.facilities.index', $vars);
	}

	public function getCreate()
	{
		$vars = [];

		return view('backend.facilities.create', $vars);
	}

	public function postCreate(PostCreateFacilityRequest $request)
	{
		$facility = Facility::create($request->all());

		$facility->features()->sync($request->get('feature_ids', []));

		$directory = public_path('images/facilities');
		$name = $request->file('image')->getClientOriginalName();

		$request->file('image')->move($directory, $name);

		$facility->clearMediaCollection();
		$facility->addMedia($directory . '/' . $name)->toMediaLibrary();

		return Redirect::route('backend.facilities.show', $facility->id)
					   ->with('messages', ['Facility was successfully created!']);
	}

	public function show($id)
	{
		$facility = Facility::findOrFail($id);
		$vars = compact('facility');

		return view('backend.facilities.show', $vars);
	}

	public function getSetImage($id)
	{
	    $facility = Facility::findOrFail($id);
	    $vars = compact('facility');
	    
	    return view('backend.facilities.set_image', $vars);
	}

	public function postSetImage(PostSetImageRequest $request, $id)
	{
//		dd($request->file('image')->getClientOriginalName());
		$facility = $request->facility;

		$directory = public_path('images/facilities');
		$name = $request->file('image')->getClientOriginalName();

		$request->file('image')->move($directory, $name);
//		$path = $request->file('image')->storeAs('images', $name, 'media');

		$facility->clearMediaCollection();
		$facility->addMedia($directory . '/' . $name)->toMediaLibrary();

		$message = 'Facility Image was successfully updated!';

		return Redirect::route('backend.facilities.show', $id)->with('messages', [$message]);
	}

	public function getEdit($id)
	{
	    $facility = Facility::findOrFail($id);
	    $vars = compact('facility');

	    return view('backend.facilities.edit', $vars);
	}

	public function postEdit(PostEditFacilityRequest $request, $id)
	{
		$facility = $request->facility;
		$facility->fill($request->all());
		$facility->save();

		$facility->features()->sync($request->feature_ids);

		$directory = public_path('images/facilities');
		$name = $request->file('image')->getClientOriginalName();

		$request->file('image')->move($directory, $name);

		$facility->clearMediaCollection();
		$facility->addMedia($directory . '/' . $name)->toMediaLibrary();

		$message = 'Facility was successfully updated!';

		return Redirect::route('backend.facilities.edit.get', $id)->with('messages', [$message]);
	}

	public function destroy(DestroyFacilityRequest $request, $id)
	{
		$facility = $request->facility;
		$facility->delete();

		$message = 'Facility (ID: ' . $id . ') successfully deleted!';

		return Redirect::route('backend.facilities.index')->with('messages', [$message]);
	}
}