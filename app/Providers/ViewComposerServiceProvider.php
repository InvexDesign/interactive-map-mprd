<?php namespace App\Providers;

use App\Http\Controllers\Backend\FacilityController;
use App\Http\Controllers\Backend\FeatureController;
use App\Http\Controllers\Backend\SettingController;
use App\Http\Controllers\Backend\UserController;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		FacilityController::composeViews();
		FeatureController::composeViews();
//		FrontendController::composeViews();
		SettingController::composeViews();
		UserController::composeViews();
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}
}
