<?php namespace App\Helpers;

class Breadcrumbs
{
	//TODO: Create views? ('breadcrumbs.main', 'breadcrumbs.crumb')
	public static function generate($crumbs, $divider = '\\')
	{
		$count = 0;
		$html = '<ul class="breadcrumbs">';
		foreach($crumbs as $name => $args)
		{
			if($count > 0)
			{
				$html .= "<li class=\"divider\">$divider</li>";
			}

			$html .= '<li>';

			if($args == null || count($args) == 0)
			{
				$html .= $name;
			}
			else
			{
				//Model ID
				if(isset($args[1]))
				{
					$href = route($args[0], $args[1]);
				}
				else
				{
					$href = route($args[0]);
				}

				if(isset($args[2]))
				{
					$href .= $args[2];
				}

				$html .= "<a href=\"$href\">$name</a>";
			}

			$html .= '</li>';

			$count++;
		}
		$html .= '</ul>';

		return $html;
	}
}