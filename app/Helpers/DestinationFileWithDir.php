<?php
/**
 * Created by PhpStorm.
 * User: asittwo
 * Date: 7/15/2015
 * Time: 8:35 AM
 */

namespace App\Helpers;

use Destination;

class DestinationFileWithDir extends Destination
{
	var $_link_text;
	var $_directory;

	function DestinationFile($filename, $directory, $link_text = null)
	{
		$this->Destination($filename);

		$this->_link_text = $link_text;
		$this->_directory = $directory;
	}

	function process($tmp_filename, $content_type)
	{
		$dest_filename = $this->_directory . $this->filename_escape($this->get_filename()) . "." . $content_type->default_extension;

		copy($tmp_filename, $dest_filename);

		$text = $this->_link_text;
		$text = preg_replace('/%link%/', 'file://' . $dest_filename, $text);
		$text = preg_replace('/%name%/', $this->get_filename(), $text);
		print $text;
	}
}

?>