<?php
/**
 * Created by PhpStorm.
 * User: asittwo
 * Date: 7/15/2015
 * Time: 8:35 AM
 */

namespace App\Helpers;


use App\Models\Setting;
use Auth;
use Carbon\Carbon;
use Faker\Factory;
use Session;

class TimezoneHelper
{
	public static function getUserTimezone($default = null)
	{
		if(Auth::user() && Auth::user()->timezone)
		{
			return Auth::user()->timezone;
		}
		elseif(Session::has('timezone') && Session::get('timezone', false))
		{
			return Session::get('timezone');
		}
		elseif($default == null)
		{
			return Setting::get('default_timezone', 'America/Chicago');
		}
		
		return $default;
	}
}